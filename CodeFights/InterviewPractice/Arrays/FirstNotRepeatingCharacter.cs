﻿/* Description
---As of 1st of August 2017---

Note: Write a solution that only iterates over the string once and uses O(1) additional memory,
since this is what you would be asked to do during a real interview.

Given a string s, find and return the first instance of a non-repeating character in it.
If there is no such character, return '_'.

Example
For s = "abacabad", the output should be
firstNotRepeatingCharacter(s) = 'c'.
There are 2 non-repeating characters in the string: 'c' and 'd'. Return c since it appears in the string first.
For s = "abacabaabacaba", the output should be
firstNotRepeatingCharacter(s) = '_'.
There are no characters in this string that do not repeat.

Input/Output

[time limit] 3000ms (cs)
[input] string s
A string that contains only lowercase English letters.

Guaranteed constraints:
1 ≤ s.length ≤ 10^5.

[output] char
The first non-repeating character in s, or '_' if there are no characters that do not repeat.
*/

using System;
using System.Collections.Generic;

namespace CodeFights.InterviewPractice.Arrays
{
    class FirstNotRepeatingCharacter : IChallenge
    {
        public void PrintResult() => Console.WriteLine(GetResult());

        private char GetResult()
        {
            string inputString = "abaxabad";

            var charOccurrences = GetCharOccurrences(inputString);
            return GetFirstNonRepeatingChar(charOccurrences, inputString);
        }

        private IDictionary<char, Character> GetCharOccurrences(string inputString)
        {
            var charDictionary = new Dictionary<char, Character>(25);
            for (int index = 0; index < inputString.Length; index++)
            {
                var character = inputString[index];
                if (charDictionary.ContainsKey(character))
                {
                    charDictionary[character].Count++;
                }
                else
                {
                    charDictionary.Add(character, new Character() { Index = index, Count = 1 });
                }
            }

            return charDictionary;
        }

        private char GetFirstNonRepeatingChar(IDictionary<char, Character> charOccurrences, string inputString)
        {
            var minIndex = inputString.Length;
            foreach (var character in charOccurrences.Values)
            {
                if (character.Count == 1 && character.Index < minIndex)
                {
                    minIndex = character.Index;
                }
            }

            return minIndex == inputString.Length ? '_' : inputString[minIndex];
        }

        private class Character
        {
            public int Index { get; set; }
            public int Count { get; set; }
        }
    }
}

/* Approach

First we iterate through the inputString chars and store each of them once in a Dictionary, together with
the number of occurrences and the first index at which we saw that char at. Since there is a fixed number
of lowercase characters, our additional space for this method is O(1).
Next we iterate through the fixed size charOccurrences Dictionary and determine which of the
non repeating characters has the lowest index.
*/
