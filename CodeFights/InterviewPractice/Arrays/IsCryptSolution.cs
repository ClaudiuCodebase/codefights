﻿/* Description
---As of 12th of August 2017---

A cryptarithm is a mathematical puzzle for which the goal is to find the
correspondence between letters and digits, such that the given arithmetic
equation consisting of letters holds true when the letters are converted to digits.

You have an array of strings crypt, the cryptarithm, and an array containing
the mapping of letters and digits, solution. The array crypt will contain three
non-empty strings that follow the structure: [word1, word2, word3], which should
be interpreted as the word1 + word2 = word3 cryptarithm.

If crypt, when it is decoded by replacing all of the letters in the cryptarithm with
digits using the mapping in solution, becomes a valid arithmetic equation containing
no numbers with leading zeroes, the answer is true. If it does not become a valid
arithmetic solution, the answer is false.

Example
For crypt = ["SEND", "MORE", "MONEY"] and solution = [['O', '0'],
                                                      ['M', '1'],
                                                      ['Y', '2'],
                                                      ['E', '5'],
                                                      ['N', '6'],
                                                      ['D', '7'],
                                                      ['R', '8'],
                                                      ['S', '9']]
the output should be isCryptSolution(crypt, solution) = true.
When you decrypt "SEND", "MORE", and "MONEY" using the mapping given in crypt,
you get 9567 + 1085 = 10652 which is correct and a valid arithmetic equation.
For crypt = ["TEN", "TWO", "ONE"] and solution = [['O', '1'],
                                                  ['T', '0'],
                                                  ['W', '9'],
                                                  ['E', '5'],
                                                  ['N', '4']]
the output should be isCryptSolution(crypt, solution) = false.
Even though 054 + 091 = 145, 054 and 091 both contain leading zeroes, meaning
that this is not a valid solution.

Input/Output

[time limit] 3000ms (cs)
[input] array.string crypt
An array of three non-empty strings containing only uppercase English letters.

Guaranteed constraints:
crypt.length = 3,
1 ≤ crypt[i].length ≤ 14.

[input] array.array.char solution
An array consisting of pairs of characters that represent the correspondence between
letters and numbers in the cryptarithm. The first character in the pair is an uppercase
English letter, and the second one is a digit in the range from 0 to 9.

Guaranteed constraints:
solution[i].length = 2,
'A' ≤ solution[i][0] ≤ 'Z',
'0' ≤ solution[i][1] ≤ '9',
solution[i][0] ≠ solution[j][0], i ≠ j,
solution[i][1] ≠ solution[j][1], i ≠ j.
It is guaranteed that solution only contains entries for the letters present in crypt
and that different letters have different values.

[output] boolean
Return true if the solution represents the correct solution to the cryptarithm crypt,
otherwise return false.
*/

using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeFights.InterviewPractice.Arrays
{
    class IsCryptSolution : IChallenge
    {
        public void PrintResult() => Console.WriteLine(GetResult());

        private bool GetResult()
        {
            var crypt = new string[] { "SEND", "MORE", "MONEY" };
            var solution = new char[][]
            {
                new[]{'O', '0'},
                new[]{'M', '1'},
                new[]{'Y', '2'},
                new[]{'E', '5'},
                new[]{'N', '6'},
                new[]{'D', '7'},
                new[]{'R', '8'},
                new[]{'S', '9' }
            };

            return IsCrypt(solution, crypt);
        }

        private bool IsCrypt(char[][] solution, string[] crypt)
        {
            var dictionary = solution.ToDictionary(key => key[0], value => value[1]);
            if (crypt.Any(w => dictionary[w[0]] == '0' && w.Length > 1))
            {
                return false;
            }

            int firstNumber = ConvertToNumber(crypt[0], dictionary);
            int secondNumber = ConvertToNumber(crypt[1], dictionary);
            int thirdNumber = ConvertToNumber(crypt[2], dictionary);

            return firstNumber + secondNumber == thirdNumber;
        }

        private int ConvertToNumber(string word, IDictionary<char, char> dictionary)
        {
            return word.Aggregate(0,
                (num, nextChar) => num * 10 + int.Parse(dictionary[nextChar].ToString()));
        }
    }
}

/* Approach

We first map the solution grid to a dictionary to make accessing the values more easily.
Then we check if there's any word which has 0 as its corresponding number. We need to also
check if that word is longer than 1 character, because 0 + something is a valid operation.
Next, for every word, we map each character to its corresponding number and then check
whether the first two resulting numbers are equal to the third one.
*/
