﻿using System.Linq;

namespace CodeFights.InterviewPractice.LinkedLists.Utils
{
    static class LinkedListUtils
    {
        static public ListNode CreateList(int[] values)
        {
            var head = new ListNode();
            if (values.Any())
            {
                head.Value = values[0];
                var current = head;
                for (int i = 1; i < values.Length; i++)
                {
                    current.Next = new ListNode() { Value = values[i] };
                    current = current.Next;
                }
            }

            return head;
        }
    }
}
