﻿/* Description
---As of 30th of July 2017---

Note: Write a solution with O(n) time complexity and O(1) additional space complexity, since this is what you
would be asked to do during a real interview.

Given an array a that contains only numbers in the range from 1 to a.length, find the first duplicate number for
which the second occurrence has the minimal index. In other words, if there are more than 1 duplicated numbers,
return the number for which the second occurrence has a smaller index than the second occurrence of the other number does.
If there are no such elements, return -1.

Example
For a = [2, 3, 3, 1, 5, 2], the output should be
firstDuplicate(a) = 3.
There are 2 duplicates: numbers 2 and 3. 
The second occurrence of 3 has a smaller index than second occurrence of 2 does, so the answer is 3.
For a = [2, 4, 3, 5, 1], the output should be
firstDuplicate(a) = -1.

Input/Output

[time limit] 3000ms (cs)
[input] array.integer a

Guaranteed constraints:
1 ≤ a.length ≤ 10^5,
1 ≤ a[i] ≤ a.length.

[output] integer
The element in a that occurs in the array more than once and has the minimal index for its second occurrence.
If there are no such elements, return -1.
*/

using System;

namespace CodeFights.InterviewPractice.Arrays
{
    class FirstDuplicate : IChallenge
    {
        public void PrintResult() => Console.WriteLine(GetResult());

        private int GetResult()
        {
            int[] numArray = { 2, 3, 1, 6, 6, 2 };
            return GetFirstDuplicate(numArray);
        }

        private int GetFirstDuplicate(int[] numArray)
        {
            foreach (int num in numArray)
            {
                // Subtract 1 to cover index out of range exception
                // and also to make use of the 0th element
                var currentElem = Math.Abs(num) - 1;
                if (numArray[currentElem] > 0)
                {
                    // Mark as visited
                    numArray[currentElem] = -numArray[currentElem];
                }
                else // We already visited it, so it's a duplicate
                {
                    return Math.Abs(num);
                }
            }

            return -1;
        }
    }
}

/* Approach

The approach here is make use of the same array in order to mark the visited values.
This way we keep the running time at O(n).
When we visit one value, we go to the array[visitedValue] and make it negative.
This way, the next time we encounter the same value, we will go again at array[visitedValue]
and check whether it's positive or negative, if it's negative, we have found a duplicate!
The part where we mark and check values is O(1) space
*/
