﻿/* Description
---As of 12th of August 2017---

Sudoku is a number-placement puzzle. The objective is to fill a 9 × 9 grid with numbers in such a way
that each column, each row, and each of the nine 3 × 3 sub-grids that compose the grid all contain all
of the numbers from 1 to 9 one time.

Implement an algorithm that will check whether the given grid of numbers represents a valid Sudoku
puzzle according to the layout rules described above. Note that the puzzle represented by grid
does not have to be solvable.

Example
For grid = [['.', '.', '.', '1', '4', '.', '.', '2', '.'],
            ['.', '.', '6', '.', '.', '.', '.', '.', '.'],
            ['.', '.', '.', '.', '.', '.', '.', '.', '.'],
            ['.', '.', '1', '.', '.', '.', '.', '.', '.'],
            ['.', '6', '7', '.', '.', '.', '.', '.', '9'],
            ['.', '.', '.', '.', '.', '.', '8', '1', '.'],
            ['.', '3', '.', '.', '.', '.', '.', '.', '6'],
            ['.', '.', '.', '.', '.', '7', '.', '.', '.'],
            ['.', '.', '.', '5', '.', '.', '.', '7', '.']]
the output should be
sudoku2(grid) = true;
For grid = [['.', '.', '.', '.', '2', '.', '.', '9', '.'],
            ['.', '.', '.', '.', '6', '.', '.', '.', '.'],
            ['7', '1', '.', '.', '7', '5', '.', '.', '.'],
            ['.', '7', '.', '.', '.', '.', '.', '.', '.'],
            ['.', '.', '.', '.', '8', '3', '.', '.', '.'],
            ['.', '.', '8', '.', '.', '7', '.', '6', '.'],
            ['.', '.', '.', '.', '.', '2', '.', '.', '.'],
            ['.', '1', '.', '2', '.', '.', '.', '.', '.'],
            ['.', '2', '.', '.', '3', '.', '.', '.', '.']]
the output should be
sudoku2(grid) = false.
The given grid is not correct because there are two 1s in the second column. Each column, each row,
and each 3 × 3 sub grid can only contain the numbers 1 through 9 one time.

Input/Output

[time limit] 3000ms (cs)
[input] array.array.char grid
A 9 × 9 array of characters, in which each character is either a digit from '1' to '9' or a period '.'.

[output] boolean
Return true if grid represents a valid Sudoku puzzle, otherwise return false. 
*/

using System;

namespace CodeFights.InterviewPractice.Arrays
{
    class Sudoku2 : IChallenge
    {
        public void PrintResult() => Console.WriteLine(GetResult());

        private bool GetResult()
        {
            char[][] grid =
            {
                new char[]{'.','.','.','.','.','.','5','.','.'},
                new char[]{'.','.','.','.','.','1','.','.','.'},
                new char[]{'.','.','.','.','.','.','.','.','.'},
                new char[]{'9','3','.','.','2','.','4','.','.'},
                new char[]{'.','.','7','.','.','.','3','.','.'},
                new char[]{'.','.','.','.','.','.','.','.','.'},
                new char[]{'.','.','.','3','4','.','.','.','.'},
                new char[]{'.','.','.','.','.','2','.','.','.'},
                new char[]{'.','.','.','.','.','5','2','.','.'}
            };

            return IsValidSudokuPuzzle(grid);
        }

        private bool IsValidSudokuPuzzle(char[][] grid)
        {
            for (int x = 0; x < grid.Length; x++)
            {
                // We will use two arrays, one to store the values 
                // encountered on each row and one for each column
                var colValuesCount = new int[10];
                var rowValuesCount = new int[10];
                for (int y = 0; y < grid.Length; y++)
                {
                    // If we are at the starting point of a 3x3 sub grid,
                    // check if there are any duplicate values
                    if (x % 3 == 0 && y % 3 == 0 && HasDuplicatesInSubGrid(grid, x, y))
                    {
                        return false;
                    }

                    // Check whether the current row or column has duplicates
                    if (HasDuplicates(grid, x, y, rowValuesCount) || HasDuplicates(grid, y, x, colValuesCount))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private bool HasDuplicates(char[][] grid, int x, int y, int[] valuesCount)
        {
            if (int.TryParse(grid[x][y].ToString(), out int num))
            {
                // If true, it means we found a duplicate
                if (valuesCount[num] == 1)
                {
                    return true;
                }
                valuesCount[num]++;
            }

            return false;
        }

        private bool HasDuplicatesInSubGrid(char[][] grid, int row, int col)
        {
            var valuesCount = new int[10];

            // Iterate only in the 3x3 sub grid
            for (int x = row; x < row + 3; x++)
            {
                for (int y = col; y < col + 3; y++)
                {
                    if (HasDuplicates(grid, x, y, valuesCount))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}

/* Approach

We iterate through the main grid once.In order to check if we have a duplicate or not on the current
row or column we use two arrays, one to store the count of visited column values and one for the row
values. For each value we visit we increment the count at the array index equal to the visited value.
We also iterate through each 3x3 sub grid and use the same technique of counting values, but this time
we only use one array for storage.
*/
