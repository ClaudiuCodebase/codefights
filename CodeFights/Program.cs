﻿using CodeFights.InterviewPractice.LinkedLists;
using System;

namespace CodeFights
{
    class Program
    {
        static void Main(string[] args)
        {
            var challenge = new RearrangeLastN();
            challenge.PrintResult();
            Console.ReadKey();
        }
    }
}
