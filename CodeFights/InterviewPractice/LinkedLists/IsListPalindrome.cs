﻿/* Description
---As of 19th of August 2017---

Note: Try to solve this task in O(n) time using O(1) additional space, where n is the
number of elements in l, since this is what you'll be asked to do during an interview.
Given a singly linked list of integers, determine whether or not it's a palindrome.

Example
For l = [0, 1, 0], the output should be
isListPalindrome(l) = true;
For l = [1, 2, 2, 3], the output should be
isListPalindrome(l) = false.

Input/Output

[time limit] 3000ms (cs)
[input] linkedlist.integer l
A singly linked list of integers.

Guaranteed constraints:
0 ≤ list size ≤ 5 · 10^5,
-10^9 ≤ element value ≤ 10^9.

[output] boolean
Return true if l is a palindrome, otherwise return false.
*/

using CodeFights.InterviewPractice.LinkedLists.Utils;
using System;

namespace CodeFights.InterviewPractice.LinkedLists
{
    class IsListPalindrome : IChallenge
    {
        public void PrintResult() => Console.WriteLine(GetResult());

        private bool GetResult()
        {
            var head = LinkedListUtils.CreateList(new int[] { 1, 2, 3, 2, 1 });

            if (head is null)
            {
                return true;
            }

            // Find the middle of the list
            var middleNode = GetMiddleNode(head);

            // Reverse the second half
            var reversedHalf = GetReversedHalf(middleNode);

            // Iterate through the first and second half
            // at once while comparing node values
            return IsPalindrome(head, reversedHalf.Next);
        }

        private ListNode GetMiddleNode(ListNode current)
        {
            var second = current;
            while (second != null && second.Next != null)
            {
                // Cover the case when list has an even number of nodes
                if (second.Next.Next != null)
                {
                    current = current.Next;
                }
                second = second.Next.Next;
            }

            return current;
        }

        private ListNode GetReversedHalf(ListNode middleNode)
        {
            ListNode prev = null;
            ListNode temp = null;
            var current = middleNode.Next;
            while (current != null)
            {
                temp = current;
                current = current.Next;
                temp.Next = prev;
                prev = temp;
            }
            middleNode.Next = temp;

            return middleNode;
        }

        private bool IsPalindrome(ListNode current, ListNode middleNode)
        {
            while (middleNode != null)
            {
                if (current.Value != middleNode.Value)
                {
                    return false;
                }
                current = current.Next;
                middleNode = middleNode.Next;
            }

            return true;
        }
    }
}

/* Approach

We first use two iterators to find the half of the list and return the corresponding
node. Then we iterate from that node to the end in order to reverse the second half
of the list. Finally, we iterate the first and second half of the list at the same
time using two iterators while comparing the node values.
We will always iterate 1.5 times through the list so our run time is O(n) and our used
space is O(1) since we use a constant number of variables.
*/
