﻿/* Description
---As of 6th of August 2017---

Note: Try to solve this task in-place (with O(1) additional memory),
since this is what you'll be asked to do during an interview.

You are given an n x n 2D matrix that represents an image. Rotate the image by 90 degrees (clockwise).

Example
For a = [[1, 2, 3],
         [4, 5, 6],
         [7, 8, 9]]
the output should be rotateImage(a) = [[7, 4, 1],
                                       [8, 5, 2],
                                       [9, 6, 3]]

Input/Output:

[time limit] 3000ms (cs)
[input] array.array.integer a

Guaranteed constraints:
1 ≤ a.length ≤ 100,
a [i].length = a.length,
1 ≤ a [i]
[j] ≤ 10^4.

[output] array.array.integer
*/

using System;

namespace CodeFights.InterviewPractice.Arrays
{
    class RotateImage : IChallenge
    {
        public void PrintResult()
        {
            var rotatedMatrix = GetResult();
            for (int x = 0; x < rotatedMatrix.Length; x++)
            {
                for (int y = 0; y < rotatedMatrix.Length; y++)
                {
                    Console.Write($"{rotatedMatrix[x][y]}  ");
                }
                Console.WriteLine();
            }
        }

        private int[][] GetResult()
        {
            int[][] arr =
            {
                new[]{1, 2, 3 },
                new[]{4, 5, 6 },
                new[]{7, 8, 9 }
            };

            return GetRotatedImage(arr);
        }

        public int[][] GetRotatedImage(int[][] arr)
        {
            // Visit each square
            for (int x = 0; x < arr.Length / 2; x++)
            {
                var relativeX = arr.Length - 1 - x;

                // Iterate through each relevant element and find
                // the other 3 corresponding elements relative to it
                for (int y = x; y < relativeX; y++)
                {
                    var relativeY = arr.Length - 1 - y;

                    int temp = arr[x][y];

                    // Move the element from the left side to the upper side
                    arr[x][y] = arr[relativeY][x];

                    // Move the element from the lower side to the left side
                    arr[relativeY][x] = arr[relativeX][relativeY];

                    // Move the element from the right side to the lower side
                    arr[relativeX][relativeY] = arr[y][relativeX];

                    // Move the element from the upper side to the left side
                    arr[y][relativeX] = temp;
                }
            }

            return arr;
        }
    }
}

/* Approach

We need to divide the matrix into multiple squares such that, for example, in a 4x4 matrix, the starting
point for the first square is at arr[0][0], for the second square at arr[1][1]. Each square has 4 sides.
One of the elements that we need to shift is on one of the sides, so we need to find the other 3 corresponding
element positions relative to the current one which we are visiting, and shift them. Then move to the next
element and repeat until we reach the half of upper side of the square, then move down to the next square.
*/
