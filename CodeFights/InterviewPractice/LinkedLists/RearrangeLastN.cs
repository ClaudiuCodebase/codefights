﻿using CodeFights.InterviewPractice.LinkedLists.Utils;
using System;

/// <summary>
/// ---Description as of 10th of December 2017---
/// Note: Try to solve this task in O(list size) time using O(1) additional space, since this is what you'll be asked during an interview.
/// Given a singly linked list of integers l and a non-negative integer n, move the last n list nodes to the beginning of the linked list.
/// 
/// Example
/// For l = [1, 2, 3, 4, 5] and n = 3, the output should be
/// rearrangeLastN(l, n) = [3, 4, 5, 1, 2];
/// For l = [1, 2, 3, 4, 5, 6, 7] and n = 1, the output should be
/// rearrangeLastN(l, n) = [7, 1, 2, 3, 4, 5, 6].
/// 
/// Input / Output
/// [time limit] 3000ms (cs)
/// 
/// [input] linkedlist.integer l
/// A singly linked list of integers.
/// 
/// Guaranteed constraints:
/// 0 ≤ list size ≤ 105,
/// -1000 ≤ element value ≤ 1000.
/// 
/// [input] integer n
/// A non-negative integer.
/// 
/// Guaranteed constraints:
/// 0 ≤ n ≤ list size.
/// 
/// [output] linkedlist.integer
/// Return l with the n last elements moved to the beginning.
/// </summary>
namespace CodeFights.InterviewPractice.LinkedLists
{
    class RearrangeLastN : IChallenge
    {
        public void PrintResult()
        {
            var result = GetResult();
            while (result != null)
            {
                Console.WriteLine(result.Value);
                result = result.Next;
            }
        }

        private ListNode GetResult()
        {
            ListNode head = LinkedListUtils.CreateList(new int[] { 1, 2, 3, 4, 5 });
            int k = 5;

            return MoveToHead(head, k);
        }

        private ListNode MoveToHead(ListNode head, int n)
        {
            if (n < 1)
            {
                return head;
            }

            var iterator = head;
            var counter = 0;
            ListNode tail = head;

            // Move through the list while keeping a reference to the n-1 node behind iterator
            while (iterator.Next != null)
            {
                iterator = iterator.Next;
                counter++;
                if (counter > n)
                {
                    tail = tail.Next;
                }
            }

            // Avoid moving nodes around if n == list.length
            if (counter >= n)
            {
                // Move the nth nodes from behind iterator to the begining of the list
                iterator.Next = head;
                head = tail.Next;
                tail.Next = null;
            }

            return head;
        }
    }
}
