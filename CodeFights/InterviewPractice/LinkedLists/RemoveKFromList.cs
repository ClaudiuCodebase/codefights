﻿/* Description
---As of 14th of August 2017---

Note: Try to solve this task in O(n) time using O(1) additional space, where n is the
number of elements in the list, since this is what you'll be asked to do during an interview.

Given a singly linked list of integers l and an integer k, remove all elements from list l
that have a value equal to k.

Example
For l = [3, 1, 2, 3, 4, 5] and k = 3, the output should be
removeKFromList(l, k) = [1, 2, 4, 5];
For l = [1, 2, 3, 4, 5, 6, 7] and k = 10, the output should be
removeKFromList(l, k) = [1, 2, 3, 4, 5, 6, 7].

Input / Output

[time limit] 3000ms (cs)
[input] linkedlist.integer l
A singly linked list of integers.

Guaranteed constraints:
0 ≤ list size ≤ 10^5,
-1000 ≤ element value ≤ 1000.

[input] integer k
An integer.

Guaranteed constraints:
-1000 ≤ k ≤ 1000.

[output] linkedlist.integer
Return l with all the values equal to k removed.
*/

using CodeFights.InterviewPractice.LinkedLists.Utils;
using System;

namespace CodeFights.InterviewPractice.LinkedLists
{
    class RemoveKFromList : IChallenge
    {
        public void PrintResult()
        {
            var result = GetResult();
            while (result != null)
            {
                Console.WriteLine(result.Value);
                result = result.Next;
            }
        }

        private ListNode GetResult()
        {
            int k = 4;
            var head = LinkedListUtils.CreateList(new int[] { 4, 3, 4, 2, 4, 4, 4 });
            return RemoveFromList(head, k);
        }

        private ListNode RemoveFromList(ListNode head, int k)
        {
            // Handle the case when the first or more values are equal to k
            while (head != null && head.Value == k)
            {
                head = head.Next;
            }

            var current = head;
            while (current != null && current.Next != null)
            {
                if (current.Next.Value == k)
                {
                    current.Next = current.Next.Next;
                    // Execute the loop again to check the new node
                    continue;
                }
                current = current.Next;
            }

            return head;
        }
    }
}

/* Approach

We first handle the case where our first value is equal to k. We do this in a while loop in case
the next node value is also equal to k. We also check for null in case all of the values are equal
to k, or the list has 0 nodes to begin with.
Next, we can safely assume that our current node value is not equal to k, so we go and check the
next nodes until we reach the end of the list.
If we find a value equal to k, we need to make the previous node point to the node located after k.
Our time cost is O(n) since we iterate only once through the list and our space cost is O(1) since
we use a fixed amount of variables.
*/
