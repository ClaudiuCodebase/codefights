﻿/* Description
---As of 4th of November 2017

Note: Your solution should have O(n) time complexity, where n is
the number of element in l, and O(1) additional space complexity,
since this is what you would be asked to accomplish in an interview.

Given a linked list l, reverse its nodes k at a time and return the
modified list. k is a positive integer that is less than or equal to
the length of l. If the number of nodes in the linked list is not a
multiple of k, then the nodes that are left out at the end should
remain as-is.
You may not alter the values in the nodes - only the nodes themselves
can be changed.

Example
For l = [1, 2, 3, 4, 5] and k = 2, the output should be
reverseNodesInKGroups(l, k) = [2, 1, 4, 3, 5];
For l = [1, 2, 3, 4, 5] and k = 1, the output should be
reverseNodesInKGroups(l, k) = [1, 2, 3, 4, 5];
For l = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11] and k = 3, the output should be
reverseNodesInKGroups(l, k) = [3, 2, 1, 6, 5, 4, 9, 8, 7, 10, 11].

Input/Output

[time limit] 3000ms (cs)
[input] linkedlist.integer l
A singly linked list of integers.
Guaranteed constraints:
1 ≤ list size ≤ 104,
-109 ≤ element value ≤ 109.

[input] integer k
The size of the groups of nodes that need to be reversed.
Guaranteed constraints:
1 ≤ k ≤ l size.

[output] linkedlist.integer
The initial list, with reversed groups of k elements.
*/

using CodeFights.InterviewPractice.LinkedLists.Utils;
using System;

namespace CodeFights.InterviewPractice.LinkedLists
{
    class ReverseNodesInKGroups : IChallenge
    {
        public void PrintResult()
        {
            var resultListHead = GetResult();
            while (resultListHead != null)
            {
                Console.WriteLine(resultListHead.Value);
                resultListHead = resultListHead.Next;
            }
        }

        public ListNode GetResult()
        {
            int k = 3;
            var head = LinkedListUtils.CreateList(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            return ReverseList(head, k);
        }

        private ListNode ReverseList(ListNode list, int k)
        {
            // The first time we reverse the list we must save the
            // returned ListNode as the new list head
            var head = ReverseSublist(k, list);
            var iterator = head;

            while (iterator != null)
            {
                // We move to the k'th node since we already reversed k nodes
                for (int i = 0; i < k - 1; i++)
                {
                    iterator = iterator.Next;
                }

                // We also verify that we have the next k nodes
                // available for reversing, if not we just return the head
                if (!IsNextListValid(k, iterator))
                {
                    return head;
                }

                var revListHead = ReverseSublist(k, iterator.Next);

                // After reversing the next k nodes we link the previous node
                // to the reversed list
                iterator.Next = revListHead;
                iterator = revListHead;
            }

            return head;
        }

        private ListNode ReverseSublist(int k, ListNode list)
        {
            var linkToFirst = list;
            var current = list;
            var prev = list;
            var next = list.Next;

            for (int i = 0; i < k - 1; i++)
            {
                current = next;
                next = next.Next;
                current.Next = prev;
                prev = current;
            }
            linkToFirst.Next = next;

            return current;
        }

        private bool IsNextListValid(int k, ListNode list)
        {
            for (int i = 0; i < k; i++)
            {
                list = list.Next;
                if (list is null)
                {
                    return false;
                }
            }

            return true;
        }
    }
}

/* Approach

Described through comments
Solution takes O(n) time (3n = n) and 0(1) additional space
*/
