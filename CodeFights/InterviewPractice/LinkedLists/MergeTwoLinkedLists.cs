﻿/* Description
---As of 19th of September 2017---

Note: Your solution should have O(l1.length + l2.length) time complexity,
since this is what you will be asked to accomplish in an interview.

Given two singly linked lists sorted in non-decreasing order, your task is to merge them.
In other words, return a singly linked list, also sorted in non-decreasing order, that
contains the elements from both original lists.

Example
For l1 = [1, 2, 3] and l2 = [4, 5, 6], the output should be
mergeTwoLinkedLists(l1, l2) = [1, 2, 3, 4, 5, 6];
For l1 = [1, 1, 2, 4] and l2 = [0, 3, 5], the output should be
mergeTwoLinkedLists(l1, l2) = [0, 1, 1, 2, 3, 4, 5].

Input / Output

[time limit] 3000ms (cs)
[input] linkedlist.integer l1
A singly linked list of integers.

Guaranteed constraints:
0 ≤ list size ≤ 10^4,
-10^9 ≤ element value ≤ 10^9.

[input] linkedlist.integer l2
A singly linked list of integers.

Guaranteed constraints:
0 ≤ list size ≤ 10^4,
-10^9 ≤ element value ≤ 10^9.

[output] linkedlist.integer
A list that contains elements from both l1 and l2, sorted in non-decreasing order.
*/

using CodeFights.InterviewPractice.LinkedLists.Utils;
using System;

namespace CodeFights.InterviewPractice.LinkedLists
{
    class MergeTwoLinkedLists : IChallenge
    {
        private ListNode resultListHead;
        private ListNode currentResultListNode;

        public void PrintResult()
        {
            GetResult();
            while (resultListHead != null)
            {
                Console.WriteLine(resultListHead.Value);
                resultListHead = resultListHead.Next;
            }
        }

        public void GetResult()
        {
            var firstList = LinkedListUtils.CreateList(new int[] { 5, 10, 15, 40 });
            var secondList = LinkedListUtils.CreateList(new int[] { 2, 3, 20 });

            while (firstList != null || secondList != null)
            {
                if (firstList != null && secondList != null && firstList.Value <= secondList.Value || secondList is null)
                {
                    AddToResultList(firstList);
                    firstList = firstList.Next;
                }
                else
                {
                    AddToResultList(secondList);
                    secondList = secondList.Next;
                }
            }
        }

        private void AddToResultList(ListNode currentNode)
        {
            if (currentResultListNode is null)
            {
                resultListHead = new ListNode() { Value = currentNode.Value };
                currentResultListNode = resultListHead;
                return;
            }

            currentResultListNode.Next = new ListNode() { Value = currentNode.Value };
            currentResultListNode = currentResultListNode.Next;
        }
    }
}

/* Approach

We compare items from the two lists and add the smallest one to the result list, until we
reach the end of both lists. We make sure to add the appropriate conditions to cover the
case when one of the lists becomes null, or was null to begin with.
Since we iterate through both lists (at most) only once, our solution is
O(l1.length + l2.length), which is the same thing as O(n).
*/
