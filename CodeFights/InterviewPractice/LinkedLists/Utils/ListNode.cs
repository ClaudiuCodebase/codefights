﻿namespace CodeFights.InterviewPractice.LinkedLists.Utils
{
    // Class provided by the website for every challenge
    class ListNode
    {
        public int Value { get; set; }
        public ListNode Next { get; set; }
    }
}
