# CodeFights

- This repo contains all the codefights challenges I've completed so far for fun
- All challenges have the requirements inside the source code file just in case the website goes out, and also the date at which I wrote the description, in case it changes on the website
- The class names resemble the actual challenge names on the website at the time of writing them
- To run a particular challenge and see its output, just replace the current class in `Program.cs` with the class name of the challenge