﻿/* Description
---As of 4th of November 2017---

You're given 2 huge integers represented by linked lists. Each linked list element
is a number from 0 to 9999 that represents a number with exactly 4 digits.
The represented number might have leading zeros. Your task is to add up these huge
integers and return the result in the same format.

Example
For a = [9876, 5432, 1999] and b = [1, 8001], the output should be
addTwoHugeNumbers(a, b) = [9876, 5434, 0].
Explanation: 987654321999 + 18001 = 987654340000.
For a = [123, 4, 5] and b = [100, 100, 100], the output should be
addTwoHugeNumbers(a, b) = [223, 104, 105].
Explanation: 12300040005 + 10001000100 = 22301040105.

Input/Output

[time limit] 3000ms(cs)
[input]
linkedlist.integer a
The first number, without its leading zeros.

Guaranteed constraints:
0 ≤ a size ≤ 10^4,
0 ≤ element value ≤ 9999.

[input] linkedlist.integer b
The second number, without its leading zeros.

Guaranteed constraints:
0 ≤ b size ≤ 10^4,
0 ≤ element value ≤ 9999.

[output] linkedlist.integer
The result of adding a and b together, returned without leading zeros in the same format.
*/

using CodeFights.InterviewPractice.LinkedLists.Utils;
using System;
using System.Numerics;

namespace CodeFights.InterviewPractice.LinkedLists
{
    class AddTwoHugeNumbers : IChallenge
    {
        public void PrintResult()
        {
            var result = GetResult();
            while (result != null)
            {
                Console.WriteLine(result.Value);
                result = result.Next;
            }
        }

        private ListNode GetResult()
        {
            var firstList = LinkedListUtils.CreateList(new int[] { 9999, 9999, 9999, 9999 });
            var secondList = LinkedListUtils.CreateList(new int[] { 1 });

            BigInteger firstNumber = ConvertListToNumber(firstList);
            BigInteger secondNumber = ConvertListToNumber(secondList);
            BigInteger sum = firstNumber + secondNumber;

            var result = new ListNode() { Value = (int)(sum % 10000) };
            sum /= 10000;
            while (sum != 0)
            {
                result = AddNodeToStart((int)(sum % 10000), result);
                sum /= 10000;
            }

            return result;
        }

        private BigInteger ConvertListToNumber(ListNode list)
        {
            BigInteger number = 0;
            while (list != null)
            {
                number = number * 10000 + list.Value;
                list = list.Next;
            }

            return number;
        }

        private ListNode AddNodeToStart(int value, ListNode head)
        {
            var newNode = new ListNode() { Value = value };
            newNode.Next = head;

            return newNode;
        }
    }
}

/* Approach

We first convert each of the two lists to BigInteger. In order to represent the leading zeros
we always multiply our number by 10000 before adding the next node value.
After summing the two resulting numbers, we extract four digits at a once from the end of the
sum (this time ignoring any leading zeros) and add them to the start of the resulting list.
*/
